$(document).ready(function () {
    $("#menu").on("click", "a", function (evt) {
        evt.preventDefault();
        let id  = $(this).attr('href');
        let top = $(id).offset().top;
        $('body, html').animate({scrollTop: top}, 1500);

    });

    $("#slideToggle").click(function(){
        $(".posts").slideToggle(500);
        if($('.mp-posts').height() === 1030) {
            $('.mp-posts').height(138);
        }
        else {
            $('.mp-posts').height(1030).delay( 1000 );
        }
        return false;
    });

    $('.home').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 700);
        return false;
    });

    $( window ).on('scroll', function() {
        if($(window).scrollTop() > $(window).height()) {
            $('.home').show("fast");
        }
        else {
            $('.home').hide("fast");
        }
      });

});
